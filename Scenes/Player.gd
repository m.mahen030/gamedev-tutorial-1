extends Area2D

var screensize

var speed = 300

onready var bullet = preload("res://Scenes/Laser.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect (). size

func _process(delta):
	var velocity = 0
	if Input.is_action_pressed("ui_right"):
		velocity = 1
	if Input.is_action_pressed("ui_left"):
		velocity = -1
	velocity = velocity * speed * delta
	position.x += velocity
	position.x = clamp(position.x, 0, screensize.x)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
